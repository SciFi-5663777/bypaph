﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading.Tasks;

namespace BypaPH_Wrapper
{
    public class BypaPH : IDisposable
    {
        [DllImport("BypaPH.dll", EntryPoint = "CreateInstance")]
        static extern IntPtr BypaPH_ctor(uint pID);
        [DllImport("BypaPH.dll", EntryPoint = "DeleteInstance")]
        static extern void BypaPH_dtor(IntPtr pInstance);
        [DllImport("BypaPH.dll", EntryPoint = "RWVM")]
        static extern bool BypaPH_RWVM(IntPtr pInstance, UIntPtr BaseAddress, [Out] byte[] Buffer, UIntPtr BufferSize, out ulong NumberOfBytesReadOrWritten,
                                       bool read = true, bool unsafeRequest = false);

        IntPtr pInstance;

        public BypaPH(int pID)
        {
            pInstance = BypaPH_ctor((uint)pID);
        }

        public bool ReadProcessMem(ulong BaseAddress, out byte[] Buffer, int BufferSize, out ulong NumberOfBytesRead)
        {
            byte[] buf = new byte[BufferSize];
            bool b = BypaPH_RWVM(pInstance, new UIntPtr(BaseAddress), buf, new UIntPtr((uint)buf.Length), out NumberOfBytesRead);

            Buffer = buf;
            return b;
        }

        public bool WriteProcessMem(ulong BaseAddress, byte[] Buffer, out ulong NumberOfBytesWittin)
        {
            return BypaPH_RWVM(pInstance, new UIntPtr(BaseAddress), Buffer, new UIntPtr((uint)Buffer.Length), out NumberOfBytesWittin, false);
        }

        public void Dispose()
        {
            BypaPH_dtor(pInstance);
        }
    }
}

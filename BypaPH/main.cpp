#include <Windows.h>
#include "BypaPH.hpp"

extern "C" __declspec(dllexport) BypaPH* CreateInstance(DWORD pID)
{
	BypaPH* p = new BypaPH(pID);
	return p;
}

extern "C" __declspec(dllexport) void DeleteInstance(BypaPH* pInstance)
{
	BypaPH* p = (BypaPH*)pInstance;
	delete p;
}

extern "C" __declspec(dllexport) bool RWVM(BypaPH* pInstance, PVOID BaseAddress, PVOID Buffer, SIZE_T BufferSize, PSIZE_T NumberOfBytesReadOrWritten = nullptr, bool read = true, bool unsafe = false)
{
	BypaPH* p = (BypaPH*)pInstance;
	NTSTATUS b = p->RWVM(p->m_hTarget, BaseAddress, Buffer, BufferSize, NumberOfBytesReadOrWritten, read, unsafe);
	return NT_SUCCESS(b);
}

BOOL WINAPI DllMain(
	_In_ HINSTANCE hinstDLL,
	_In_ DWORD     fdwReason,
	_In_ LPVOID    lpvReserved
)
{
	return TRUE;
}